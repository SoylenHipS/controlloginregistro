package cl.duoc.ejerciciologin;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cl.duoc.ejerciciologin.Entidades.Registro;

public class LoginActivity extends AppCompatActivity{

    private Button btnIngresar;
    private EditText UserName, Password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        UserName = (EditText)findViewById(R.id.UserName);
        Password = (EditText)findViewById(R.id.Password);
        btnIngresar = (Button)findViewById(R.id.btnIngresar);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.btnIngresar) {
                    if (UserName.getText().toString().equals("Soylen") && Password.getText().toString().equals("Soylen123")) {
                        Toast.makeText(LoginActivity.this, "Bienvenido " + UserName.getText().toString()+" Ya estas logueado", Toast.LENGTH_SHORT).show();
                        Intent s = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(s);

                    } else {
                        Toast.makeText(LoginActivity.this, "Error, Debe registrarse", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(LoginActivity.this, RegistroActivity.class);
                        startActivity(i);
                    }
                }
            }
        });

    }


}
