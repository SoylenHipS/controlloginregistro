package cl.duoc.ejerciciologin;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;
import java.util.logging.ErrorManager;

import cl.duoc.ejerciciologin.BD.AlmacenRegistro;
import cl.duoc.ejerciciologin.Entidades.Registro;

public class RegistroActivity extends AppCompatActivity {

    private EditText Usuario, Nombres, Apellidos, Rut, Password, PasswordRepetir;
    private Button btnIngresar,FechaNacimiento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        Usuario = (EditText)findViewById(R.id.Usuario);
        Nombres = (EditText)findViewById(R.id.Nombres);
        Apellidos = (EditText)findViewById(R.id.Apellidos);
        Rut = (EditText)findViewById(R.id.Rut);
        Password = (EditText)findViewById(R.id.Password);
        PasswordRepetir = (EditText)findViewById(R.id.PasswordRepetir);
        FechaNacimiento = (Button) findViewById(R.id.btnFechaNacimiento);
        btnIngresar = (Button)findViewById(R.id.btnIngreaar);

        FechaNacimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().show();
            }
        });



        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                String MensajeError = "";
                if (Usuario.getText().toString().length()<1){
                    MensajeError += "Ingrese usuario \n" ;
                }
                if (Nombres.getText().toString().length() < 1) {
                    MensajeError += "Ingrese Nombre \n";
                }
                if (Apellidos.getText().toString().length() < 1) {
                    MensajeError += "Ingrese apellidos \n";
                }

                if (!isValidarRut(Rut.getText().toString())){
                    MensajeError +="Ingrese un rut valido \n";
                    }
                if(!PasswordRepetir.getText().toString().equals(Password.getText().toString()))
                {
                    MensajeError += "Claves no coinsiden \n";

                }

                if(MensajeError.equals(""))
                {
                    Registro nuevoForm = new Registro();
                    nuevoForm.setUsuraio(Usuario.getText().toString());
                    nuevoForm.setNombres(Nombres.getText().toString());
                    nuevoForm.setApellidos(Apellidos.getText().toString());
                    nuevoForm.setRut(Rut.getText().toString());
                    nuevoForm.setFechaNacimiento(FechaNacimiento.getText().toString());
                    nuevoForm.setPassword(Password.getText().toString());
                    AlmacenRegistro.AgregarRegistro(nuevoForm);
                    Toast.makeText(RegistroActivity.this, "Registro Correcto", Toast.LENGTH_SHORT).show();
                    Intent l = new Intent(RegistroActivity.this, LoginActivity.class);
                    startActivity(l);

                }
                else{
                    Toast.makeText(RegistroActivity.this, MensajeError, Toast.LENGTH_SHORT).show();
                }


            }


            private boolean isValidarRut(String rut) {
                boolean validacion = false;
                try {
                    rut =  rut.toUpperCase();
                    rut = rut.replace(".", "");
                    rut = rut.replace("-", "");
                    int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

                    char dv = rut.charAt(rut.length() - 1);

                    int m = 0, s = 1;
                    for (; rutAux != 0; rutAux /= 10) {
                        s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
                    }
                    if (dv == (char) (s != 0 ? s + 47 : 75)) {
                        validacion = true;
                    }

                } catch (java.lang.NumberFormatException e) {
                } catch (Exception e) {
                }
                return validacion;
            }

        }

    );
    }



    private DatePickerDialog getDialog() {
        Calendar c = Calendar.getInstance();
        int anio = c.get(Calendar.YEAR);
        int mes = c.get(Calendar.MONTH);
        int dia = c.get(Calendar.DAY_OF_MONTH);


        return new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                FechaNacimiento.setText(year + "/" + formatDate((month + 1)) + "/" + formatDate(dayOfMonth));
            }
        }, anio, mes, dia);

    }

    private String formatDate(int value) {

        return (value > 9 ? value + "" : "0" + value);

    }
}
