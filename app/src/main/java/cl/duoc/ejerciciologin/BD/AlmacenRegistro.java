package cl.duoc.ejerciciologin.BD;

import java.util.ArrayList;

import cl.duoc.ejerciciologin.Entidades.Registro;

/**
 * Created by DUOC on 25-03-2017.
 */

public class AlmacenRegistro {
    private static ArrayList<Registro> registros = new ArrayList<>();


    public static void AgregarRegistro(Registro registro){
        registros.add(registro);
    }

    public static ArrayList<Registro> getRegistros(){
        return registros;
    }
}
